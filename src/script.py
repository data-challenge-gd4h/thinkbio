import pandas as pd
import numpy as np
import zipfile
import os
from pathlib import Path
import tempfile
import os

def rename_file_without_space(path_to_zipfile):

    source_z = zipfile.ZipFile(path_to_zipfile, 'r')

    # Définir un fichier cible
    zip_path, base_for_target_name = os.path.split(path_to_zipfile)
    target_name = base_for_target_name.replace('.zip', '_rename.zip')
    target_path = zip_path + '/' + target_name
    target_z = zipfile.ZipFile(target_path, 'w', zipfile.ZIP_DEFLATED)

    # Renommer les fichiers qui contiennent des espaces
    for file in source_z.filelist:
        if ' ' in file.filename:
            newfilename = file.filename.replace(" ", "_")
            target_z.writestr(newfilename, source_z.read(file.filename))
        else:
            target_z.writestr(file.filename, source_z.read(file.filename))

    source_z.close()

    return target_path, target_z


def open_zipfile_as_df(path_to_zipfile, target_dataframe):

    target_path, target_z = rename_file_without_space(path_to_zipfile)
    
    all_files = target_z.infolist()
    
    for all_file in all_files:
        all_name = all_file.filename
        
        if all_name.endswith ('.csv'):
            fname = all_name.split('.')[0]
            # Définir un nom plus court pour les dataframes
            fname = fname.replace('BNVD_TRACABILITE_20221016_ACHAT_', 'df_')

            # Créer un dataframe pandas avec le nom correspondant au fichier csv 
            exec("{} = pd.read_csv(target_z.open(all_name), delimiter=';', low_memory=False)".format(fname))

            # Créer des dataframe pour les données avec codes postaux spécifique pour les substances et les produits
            if target_dataframe == "produits":
                if 'CP_PRODUIT' in fname:
                    if 'df_target' not in locals():
                        df_target = pd.read_csv(target_z.open(all_name), delimiter=';', low_memory=False)
                    else:
                        df_target = pd.concat([df_target, pd.read_csv(target_z.open(all_name), delimiter=';', low_memory=False)], ignore_index=True)
            
            if target_dataframe == "substances":
                if 'CP_SUBSTANCE' in fname:
                    if 'df_target' not in locals():
                        df_target = pd.read_csv(target_z.open(all_name), delimiter=';', low_memory=False)
                    else:
                        df_target = pd.concat([df_target, pd.read_csv(target_z.open(all_name), delimiter=';', low_memory=False)], ignore_index=True)  

    # Fermer le fichier zip
    target_z.close()
    # Supprimer le fichier zip créé
    target_path = Path(target_path)
    try:
        target_path.unlink()
    except OSError as e:
        print(f"Error:{ e.strerror}")

    return df_target


def dataframe_par_commune(path_to_zipfile, target_dataframe, codes_et_surfaces_agricoles):
    
    print("Etape 1/5: Ouverture du fichier zip")
    df_target = open_zipfile_as_df(path_to_zipfile, target_dataframe)

    print("Etape 2/5: Ouverture du fichier avec codes postaux, codes insee et surfaces agricoles")
    df_codes_et_surfaces_agricoles = pd.read_csv(codes_et_surfaces_agricoles, delimiter=",")

    # Ajouter les superficies agricoles pour chaque code postal
    print("Etape 3/5: Création du jeu de données")
    df_merge = pd.merge(df_target,
                        df_codes_et_surfaces_agricoles,
                        how='left',
                        left_on='code_postal_acheteur',
                        right_on='code_postal')
    
    # Supprimer colonne en double
    df_merge.drop(columns=['code_postal'], inplace=True)

    # Définir le nom de la colonne quantite
    if target_dataframe == "produits":
        quantite = "quantite_produit"
    if target_dataframe == "substances":
        quantite = "quantite_substance"


        # Changer les valeurs 'nc' des quantités pour faire ajuster les quantités par communes
    df_merge[quantite] = np.where((df_merge[quantite] == "nc"),
                                  np.nan,
                                  df_merge[quantite])

    # changer les quantités en fonction de la surface agricole
    print("Etape 4/5: Ajustement des quantités en fonction des surfaces agricoles")
    df_merge['quantite_par_commune'] = np.where((df_merge['Superficie_Territoires_agricoles'].notna()
                                                 & df_merge['Superficie_Territoires_agricoles'] > 0.),
                                                 ((df_merge[quantite].astype(float)
                                                  * df_merge['Superficie_Territoires_agricoles'])
                                                  / df_merge['Superficie_agricole_par_code_postal']),
                                                  df_merge[quantite].astype(float))

    df_merge.loc[(((df_merge['Superficie_Territoires_agricoles'] == 0.)
                   & (df_merge['Nombre_commune_par_code_postal'] != 1))),
                   'quantite_par_commune'] = 0.00

    df_merge.loc[(((df_merge['Superficie_Territoires_agricoles'].isna())
                   & (df_merge['Nombre_commune_par_code_postal'] > 1))),
                   'quantite_par_commune'] = (df_merge[quantite].astype(float)
                                              / df_merge['Nombre_commune_par_code_postal'])

    df_merge.loc[(((df_merge['Superficie_Territoires_agricoles'] == 0.)
                   & (df_merge['Superficie_agricole_par_code_postal'] == 0.)
                   & (df_merge['Nombre_commune_par_code_postal'] > 1))),
                   'quantite_par_commune'] = (df_merge[quantite].astype(float)
                                              / df_merge['Nombre_commune_par_code_postal'])
    

    df_merge.drop([quantite,
                   'Nombre_commune_par_code_postal',
                   'Superficie_agricole_par_code_postal'],
                   axis=1,
                   inplace=True)

    df_par_commune = df_merge.sort_values(by=['code_postal_acheteur', 'code_commune_insee'])


    return df_par_commune

def set_dtypes(df, target_dataframe):
    if target_dataframe == "produits":
        df['annee'] = df['annee'].astype('category')
        df['code_postal_acheteur'] = df['code_postal_acheteur'].astype('category')
        df['amm'] = df['amm'].astype(str)
        df['quantite_par_commune'] = df['quantite_par_commune'].astype('float32')
        df['conditionnement'] = df['conditionnement'].astype('category')
        df['eaj'] = df['eaj'].astype('category')
        df['achat_etranger'] = df['achat_etranger'].astype('category')
        df['code_commune_insee'] = df['code_commune_insee'].astype('category')
        df['nom_de_la_commune'] = df['nom_de_la_commune'].astype('category')
        df['coordonnees_geographiques'] = df['coordonnees_geographiques'].astype('category')
        df['Superficie_Territoires_agricoles'] = df['Superficie_Territoires_agricoles'].astype('float32')
    elif target_dataframe == "substances":
        df['annee'] = df['annee'].astype('category')
        df['code_postal_acheteur'] = df['code_postal_acheteur'].astype('category')
        df['amm'] = df['amm'].astype(str)
        df['quantite_par_commune'] = df['quantite_par_commune'].astype('float32')
        df['substance'] = df['substance'].astype('category')
        df['cas'] = df['cas'].astype('category')
        df['classification'] = df['classification'].astype('category')
        df['classification_mention'] = df['classification_mention'].astype('category')
        df['achat_etranger'] = df['achat_etranger'].astype('category')
        df['code_commune_insee'] = df['code_commune_insee'].astype('category')
        df['nom_de_la_commune'] = df['nom_de_la_commune'].astype('category')
        df['coordonnees_geographiques'] = df['coordonnees_geographiques'].astype('category')
        df['Superficie_Territoires_agricoles'] = df['Superficie_Territoires_agricoles'].astype('float32')

    return df


def save_dataframe(df_to_save,
                   data_over_year,
                   path_to_save,
                   file_name,
                   file_format='feather',
                   update=False,
                   target_dataframe=" "
                   ):
    
    print("Etape 5/5 : Sauvegarde du jeu de données créé")

    if update:
        if data_over_year.endswith('.csv'):
            print(" File end with csv")
            df_over_year = pd.read_csv(data_over_year, delimiter=',', low_memory=False)
        elif data_over_year.endswith('.feather'):
            print(" File end with feather")
            df_over_year = pd.read_feather(data_over_year)
        else:
            print("Format de fichier non supporté")

        df_concat = pd.concat([df_over_year, df_to_save], ignore_index=True)
        df_concat = set_dtypes(df_concat, target_dataframe=target_dataframe)
        df_sorted = df_concat.sort_values(by=['code_postal_acheteur', 'code_commune_insee', 'annee'])
        print(" Taille dataframe avec historique créé: ", df_sorted.shape)
        
        if file_format == 'feather':
            df_sorted.reset_index(drop=True).to_feather(path_to_save + file_name)
        else:
            df_sorted.to_csv(path_to_save + file_name)
    else:
        df_to_save = set_dtypes(df_to_save, target_dataframe=target_dataframe)

        if file_format == 'feather':
            df_to_save.reset_index(drop=True).to_feather(path_to_save + file_name)
        else:
            df_to_save.to_csv(path_to_save + file_name, index=False)

if __name__ == "__main__":

    #----------PARAMETRES A PRECISER-------------------

    # Indiquer le chemin d'accès au repertoire contenant les données à utiliser
    path_to_zipfile = '../data/BNVD_TRACABILITE_20221018_ACHAT_2021.zip'

    # Indiquer si jeu de données sur les produits ("produits") ou les substances ("substances")
    target_dataframe = "produits"

    # Indiquer où se trouve le fichier csv contenant les codes postaux, codes insee des communes et superficies agricoles
    codes_et_surfaces_agricoles = "../artifacts/superficie_agricole_par_commune.csv"

    # Indiquer où sauvegarder le jeu de données créé
    path_to_save = '../data_crees/'
    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

    # Indiquer si création juste pour une année (False) ou mise à jour de l'historique (True)
    update = False

    # Indiquer le nom du fichier avec les données des années précédentes
    data_over_year = '../data_crees/df_substances_2013_2020.feather'

    # Indiquer le format et le nom du fichier créé
    file_format = 'feather'  # 'feather' ou 'csv'
    file_name = 'df_substances_2021.feather'  # Doit inclure '.feather' ou '.csv'

    #---------------------------------------------------------


    df_to_save = dataframe_par_commune(path_to_zipfile, target_dataframe, codes_et_surfaces_agricoles)
    print(" Taille dataframe de l'année créé: ", df_to_save.shape)

    save_dataframe(df_to_save,
                   data_over_year,
                   path_to_save,
                   file_name,
                   file_format=file_format,
                   update=update,
                   target_dataframe=target_dataframe
                   )
    print("Done")