# Installation Guide

## Data Collection

It is possible to access the data about the purchase of phytopharmaceutical products on:
    - https://data.eaufrance.fr/jdd/a69c8e76-13e1-4f87-9f9d-1705468b7221
This zip file is the Vintage 2022 for year of purchase 2021. 
You can download newer or older files on this page :
    - https://data.eaufrance.fr/geosource/srv/fre/catalog.search#/metadata/a69c8e76-13e1-4f87-9f9d-1705468b7221
The zipfile can be save in your local repository (/data). 
The pathway to this local repository is specified in the script.py file (path_to_zipfile).

## Dependencies

List the dependencies necessary to the project so that it can run locally :
- the language used in this project is Python. The script is running on Python 3.11.2
- The libraries and packages are listed in the requirements.txt file. They are:
    * pandas==2.0.0
    * numpy==1.24.2
    * matplotlib==3.7.1
    * seaborn==0.12.2
    * missingno==0.5.2
    * pyarrow==11.0.0
    * plotly==5.14.1
    * nbformat==5.8.0
- You can install them manually or run setup.py
- You also need a csv file with the agricultural area (superficie_agricole_par_commune.csv) which is available in /artifacts/ folder. This file has been created using the data from 2018.

## Development

To create a dataframe with new PPP purchase order, you can run script.py in the environnement with the dependencies describe above. It will create a feather dataframe in a local folder.

## Production

In the README.md file, more informations are available about the file in this repository.