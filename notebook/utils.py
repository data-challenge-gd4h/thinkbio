import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def hist_distrib(df, variable="substance", bins=100, unit_label="unité"):
    """Create a distribution histogram for one variable
    
    return a plot"""

    # Initiate figure
    fig = plt.figure(figsize=(10, 6))

    # Plot the histogram
    ax1 = fig.add_subplot(1, 2, 1)
    ax1 = sns.histplot(x=variable,
                       data=df,
                       bins=bins,
                       color="dimgrey")
    ax1.set_xlabel("{var}".format(var=unit_label))
    ax1.set_ylabel("Occurrences")
    ax1.set_title("{var}".format(var=variable))

    # Plot the boxplot
    ax3 = fig.add_subplot(1, 2, 2)
    ax3 = sns.boxplot(data=df,
                      y=variable,
                      color="dimgrey")
    ax3.set_ylabel("{var}".format(var=unit_label))

    plt.show()


def hist_distrib_cat(df, feature, label_rotation=False):
    """ Fonction permettant d'afficher un histogramme de distributiion

    Parameters
    -----------
    df: dataframe
    feature: string, Nom de la variable à considérer

    Returns
    -------
    Affiche un histogramme de distribution de la variable
    """
    fig = plt.figure(figsize=(6, 5))
    ax = plt.axes()
    s = sns.barplot(x=df[feature].value_counts()[:15].index,
                    y=df[feature].value_counts()[:15].values,
                    ax=ax)
    ax.set_ylabel("Number of occurrences")
    ax.set_xlabel("{var}".format(var=feature))
    ax.set_title("{var}".format(var=feature))

    if(label_rotation):
        s.set_xticklabels(s.get_xticklabels(), rotation=90)
    plt.show()