# Challenge GD4H - ThinkBio

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a>

## ThinkBio

Numerous environmental data exist in specific temporal and spatial grids. This challenge aims to systematize the matching between databases at a consistent level, in a documented way and by facilitating the expansion of this matching work to new data sources.

The aim of the project is to enable analyses of the impact of the use of phytosanitary products on education and health variables.

## **Documentation**

For data on the purchase of phytopharmaceutical products, a script was created to provide a dataset listing the purchase of phytopharmaceutical products by commune. The available datasets report the quantities of phytopharmaceutical products purchased per postcode per year. The script enables these data sets to be scaled down to the commune level. Where the same zip code is associated with several communes, the quantities of products/substances have been calculated so as to be proportional to the agricultural area of the communes with the zip code. The agricultural surface area of each commune was extracted from Corine Land Cover data, separating commune surface areas into 5 categories (https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0).

An exploratory analysis of the 2021 data was carried out in the Data_phyto_EDA notebook. The notebook EDA_produits_over_year provides a brief exploration of the file created with the script, grouping data from 2013 to 2021.

### **Installation**

The source of purchasing data for plant protection products, the environment used and the dependencies used are described in the file [Installation Guide](/INSTALL.md)

### **Usage**

Once the environment has been installed, the solution envisaged for purchasing plant protection products corresponds to a script with certain parameters to be adjusted. These are located towards the end of the script below "if name == "main":" and are:

* the path to the local directory containing the downloaded zip file
* "substances" or "products" to indicate whether the dataset to be created concerns plant protection products or the substances that make them up.
* the path to the directory containing a csv file with agricultural areas by commune
* the path to the directory where the created dataset is to be saved
* "True" or "False" for update to indicate whether the dataset is created for one year (False) or to update an existing file (True). If update = True, you need to specify the path to the directory containing the file to be updated the format and name of the file to be created. It is possible to create csv or feather files. 
* Given the size of the files created, files in feather format will be easier to handle and lighter.

Once all these parameters have been set, simply run the script.

The streamlit webapp is launched from the dashboard folder

```bash
cd dashboard
streamlit run dashboard.py
```

### **Branch update**

The main branch is equivalent to the donnees_phyto branch, finalized and usable. The other branches, donnees_meteo and donnees_spatiales, contain codes that have not been finalized and cannot be used as they stand.

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
