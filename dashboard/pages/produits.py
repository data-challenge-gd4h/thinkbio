# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:47:20 2022

@author: virgi
"""


import pandas as pd
import os
import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.express as px
import seaborn as sns
import os.path
import geopandas as gpd
import folium
from streamlit_folium import folium_static
import sys



###############################################################
# Definir les fonctions specifiques
##############################################################


@st.cache_resource
def load_data_produit():
    """
    Fonction pour charger le jeu de données contenant les données sur les produits
    
    return: a pandas dataframe
    """
    df_2013 = pd.read_feather('data/produits/df_produits_2013.feather')
    df_2014 = pd.read_feather('data/produits/df_produits_2014.feather')
    df_2015 = pd.read_feather('data/produits/df_produits_2015.feather')
    df_2016 = pd.read_feather('data/produits/df_produits_2016.feather')
    df_2017 = pd.read_feather('data/produits/df_produits_2017.feather')
    df_2018 = pd.read_feather('data/produits/df_produits_2018.feather')
    df_2019 = pd.read_feather('data/produits/df_produits_2019.feather')
    df_2020 = pd.read_feather('data/produits/df_produits_2020.feather')
    df_2021 = pd.read_feather('data/produits/df_produits_2021.feather')

    return df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021

@st.cache_resource
def load_data_global():
    """
    Fonction pour charger le jeu de données contenant les données globales sur les produits
    
    return: a pandas dataframe
    """
    df_produits = pd.read_feather('data/produits/df_produits_global.feather')

    return df_produits


@st.cache_resource
def load_geo_data():
    df_geo = gpd.read_file('data/communes-avec-outre-mer.geojson')
    return df_geo

@st.cache_data
def load_data_vue_ensemble(df):
    """
    Fonction pour créer:
     - un dataframe avec le nombre d'AMM par année
     - un dataframe avec les quantités par année
    
    return 2 dataframes
    """

    list_annees = df['annee'].unique().tolist()
    list_nb_amm = []
    list_quantite = []

    for i in range(len(list_annees)):
        print(list_annees[i])
        df_tmp = df.loc[df['annee'] == list_annees[i]]
        df_tmp_amm = df_tmp.groupby('amm',
                                    as_index=False).count()
        list_nb_amm.append(df_tmp_amm.shape[0])
        quantite = df_tmp['quantite_par_commune'].astype(float).sum()
        list_quantite.append(quantite)

    df_amm = pd.DataFrame(list(zip(list_annees,
                                list_nb_amm)), 
                         columns =["Années", "Nombre d'AMM"])
    
    df_quantite = pd.DataFrame(list(zip(list_annees,
                                        list_quantite)),
                               columns =["Années", "Quantité de produits"])
    
    return df_amm, df_quantite

def threshold_for_map(df):
    # Créer une liste avec toute les quantités pour le produit
    list_toute_les_quantite = df["quantite_par_ha_agricole"].sort_values().tolist()

    # Enlever de la liste 1 % des valeurs les plus grandes (dont les valeurs aberrantes)
    stop = int(len(list_toute_les_quantite) - (len(list_toute_les_quantite) * 0.01))
    list_scale = list_toute_les_quantite[0: stop]

    # Créer une première liste d'intervalle pour la carte
    threshold_scale, step = np.linspace(0, list_scale[-1], 10, retstep=True)

    # changer les 1 % des valeurs les plus grandes par une valeur arbitraire pour la carte
    df.loc[df["quantite_par_ha_agricole"] > threshold_scale[-1],
           "quantite_par_ha_agricole"] =( threshold_scale[-1] + step)
    
    # Ajouter le dernier interval à l'échelle de la carte
    threshold_scale = np.append(threshold_scale, df["quantite_par_ha_agricole"].max())

    return threshold_scale

def show_map(df, df_geo, my_map, select_year=None, threshold_scale=None):
    if select_year:
        df_year = df.loc[df["annee"] == select_year]
    
    else:
        df_year = df
        df_for_map = pd.merge(left=df_year,
                          right=df_geo,
                          how='left',
                          left_on="code_commune_insee",
                          right_on="code")
    
    df_for_map = df_for_map.dropna(subset=["geometry"])
    gdf_for_map = gpd.GeoDataFrame(df_for_map, geometry='geometry')

        
    choro = folium.Choropleth(geo_data=gdf_for_map,
                              name='choropleth',
                              data=gdf_for_map,
                              columns=["code_commune_insee", "quantite_par_ha_agricole"],
                              key_on='feature.properties.code_commune_insee',
                              threshold_scale=threshold_scale,
                              fill_color='RdPu',
                              fill_opacity=1,
                              line_opacity=0.2,
                              legend_name=(" Quantité par hectar de surface agricole (Kg)"),
                              highlight=False).add_to(my_map)

    folium.LayerControl().add_to(my_map)

    choro.geojson.add_child(folium.features.GeoJsonTooltip(['code_commune_insee',
                                                            'quantite_par_commune',
                                                            'Superficie_Territoires_agricoles']))
    folium_static(my_map)  


def intro():
    import streamlit as st

    html_temp = """
                <div style="background-color: gray; padding:10px; border-radius:10px">
                <h1 style="color: white; text-align:center">Achats de produits phytopharmaceutiques en France</h1>
                </div>
                """
    st.markdown(html_temp, unsafe_allow_html=True)
    st.write(" ")
    st.sidebar.success("Sélectionner une analyse ci-dessus")

    # Afficher figures de la vue d'ensemble
    with st.spinner("Chargement de la vue d'ensemble..."):
        df_global = load_data_global()

        fig1 = px.bar(df_global,
                      x="Années",
                      y="Nombre d'AMM")
        fig1.update_layout(title="Nombre d'AMM par année",
                           title_x=0.3,
                           yaxis_range=[0,3000])

        fig2 = px.bar(df_global,
                      x="Années",
                      y="Quantité")
        fig2.update_layout(title="Quantité de produits achetés par année",
                           title_x=0.3,
                           yaxis_range=[0, df_global["Quantité"].max()])

        tab1, tab2 = st.tabs(["Nombre d'amm", "Quantité de produits"])
        with tab1:
            st.plotly_chart(fig1, theme='streamlit', use_container_width=True)
        with tab2:
            st.plotly_chart(fig2, theme='streamlit', use_container_width=True)


def analyse_par_produit():

    html_temp = """
                <h1 style="color: white; text-align:center">Analyse par produit acheté</h1>
                """
    st.markdown(html_temp, unsafe_allow_html=True)
    st.write(" ")

    st.text_input("Renseigner le numéro d'autorisation de mise sur le marché (AMM) du produit", key="amm")
    amm_id = st.session_state.amm

    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_produit()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    list_quantite = []
    list_annees = []
    list_unit = []

    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[list_df[i]["amm"] == amm_id]
        quantite = df_tmp[[ "quantite_par_commune"]].sum().tolist()
        list_quantite.append(quantite[0])
        list_annees.append(df_tmp['annee'].unique().tolist()[0])
        list_unit.append(df_tmp['conditionnement'].unique().tolist()[-1])

    df_fig = pd.DataFrame(list(zip(list_annees,
                                   list_quantite,
                                   list_unit,)),
                                   columns =["Années", "Quantité", "Conditionnement"])
    
    fig = px.bar(df_fig,
                 x="Années",
                 y="Quantité")
    fig.update_layout(title_text="Quantité de produits (amm = {amm})".format(amm=amm_id),
                      title_x=0.25,
                      xaxis_title="Années",
                      yaxis_title="Quantité de {amm} ({unit})".format(amm=amm_id,
                                                                      unit=df_fig["Conditionnement"].unique().tolist()[0]),
                      yaxis_range=[0, df_fig["Quantité"].max()])

    st.plotly_chart(fig, use_container_width=True)

    #Pour afficher la carte
    df_geo = load_geo_data()

    map_produit = folium.Map(location=[47., 1],
                             zoom_start=5)
    
    html_temp = """
                <h3 style="color: white; text-align:center">Répartition géographique des achats du produit</h3>
                """
    st.markdown(html_temp, unsafe_allow_html=True)

    select_year = st.select_slider("Quelle année voulez-vous afficher?",
                                   options=list_annees)

    if select_year == 2013:
        df_tmp = df_2013.loc[df_2013["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    if select_year == 2014:
        df_tmp = df_2014.loc[df_2014["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)
    
    if select_year == 2015:
        df_tmp = df_2015.loc[df_2015["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)
    
    if select_year == 2016:
        df_tmp = df_2016.loc[df_2016["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)
    
    if select_year == 2017:
        df_tmp = df_2017.loc[df_2017["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    if select_year == 2018:
        df_tmp = df_2018.loc[df_2018["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    if select_year == 2019:
        df_tmp = df_2019.loc[df_2019["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    if select_year == 2020:
        df_tmp = df_2020.loc[df_2020["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    if select_year == 2021:
        df_tmp = df_2021.loc[df_2021["amm"] == amm_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                               / df_tmp["Superficie_Territoires_agricoles"])
        threshold_scale = threshold_for_map(df_tmp)
        show_map(df_tmp,
                 df_geo,
                 map_produit,
                 threshold_scale=threshold_scale)

    st.write(""":warning: La dernière gradation de l'échelle de couleurs
                 représente les 1% de quantités de substances les plus importantes""")


def analyse_par_commune():
    html_temp = """
                <h1 style="color: white; text-align:center">Analyse par commune</h1>
                """
    st.markdown(html_temp, unsafe_allow_html=True)


    st.text_input("Renseigner le code insee de la commune", key="code")
    code_insee = st.session_state.code

    # Charger les données
    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_produit()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    #Aggreger les données
    list_quantite = []
    list_nb_amm = []
    list_annees = []
    list_nom = []
 
    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[list_df[i]["code_commune_insee"] == code_insee]
        nb_amm = len(df_tmp["amm"].unique().tolist())
        list_nb_amm.append(nb_amm)
        quantite = df_tmp[["quantite_par_commune"]].sum().tolist()
        list_quantite.append(quantite[0])
        list_annees.append(df_tmp['annee'].unique().tolist()[0])
        list_nom.append(df_tmp['nom_de_la_commune'].unique().tolist()[-1])

    df_amm = pd.DataFrame(list(zip(list_annees,
                                   list_nb_amm,
                                   list_quantite,
                                   list_nom)),
                                   columns =["Années", "Nombre d'AMM", "Quantité", "Commune"])

    fig1 = px.bar(df_amm,
                  x="Années",
                  y="Nombre d'AMM")
    fig1.update_layout(title="Nombre de produits achetés par année à {var}".format(var=df_amm["Commune"].unique().tolist()[0]),
                       title_x=0.3,
                       yaxis_range=[0,df_amm["Nombre d'AMM"].max()])

    fig2 = px.bar(df_amm,
                  x="Années",
                  y="Quantité")
    fig2.update_layout(title="Quantité de produits acheté par année à {var}".format(var=df_amm["Commune"].unique().tolist()[0]),
                       title_x=0.3,
                       yaxis_range=[0, df_amm["Quantité"].max()])

    tab1, tab2 = st.tabs(["Nombre d'AMM", "Quantité de produits"])
    with tab1:
        st.plotly_chart(fig1, theme='streamlit', use_container_width=True)
    with tab2:
        st.plotly_chart(fig2, theme='streamlit', use_container_width=True)


def analyse_par_commune_produit():
    html_temp = """
                <h1 style="color: white; text-align:center">Analyse par commune et par produit</h1>
                """
    st.markdown(html_temp, unsafe_allow_html=True)

    st.text_input("Renseigner le numéro d'autorisation de mise sur le marché (AMM) du produit", key="amm")
    amm_id = st.session_state.amm

    st.text_input("Renseigner le code insee de la commune", key="code")
    code_insee = st.session_state.code

   # Charger les données
    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_produit()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    #Aggreger les données
    list_quantite = []
    list_annees = []
    list_unit = []
    list_nom_commune = []

    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[(list_df[i]["code_commune_insee"] == code_insee)
                                & (list_df[i]["amm"] == amm_id)]
        
        if df_tmp.shape[0] != 0:
            quantite = df_tmp["quantite_par_commune"].sum()
            list_quantite.append(quantite)
            list_annees.append(df_tmp["annee"].unique().tolist()[0])
            list_unit.append(df_tmp['conditionnement'].unique().tolist()[0])
            list_nom_commune.append(df_tmp['nom_de_la_commune'].unique().tolist()[0])

    df_fig = pd.DataFrame(list(zip(list_annees,
                                   list_quantite,
                                   list_nom_commune,
                                   list_unit)),
                                   columns =["Années", "Quantité", "Commune", "Conditionnement"])

    fig = px.bar(df_fig,
                 x="Années",
                 y="Quantité")
    fig.update_layout(title_text="Quantité de {produit} à {commune}".format(produit=amm_id,
                                                                            commune=df_fig["Commune"].unique().tolist()[0]),
                       title_x=0.25,
                       xaxis_title="Années",
                       yaxis_title="Quantité de produits ({unit})".format(unit=df_fig["Conditionnement"].unique().tolist()[0]),
                       yaxis_range=[0, df_fig["Quantité"].max()])
    st.plotly_chart(fig, theme='streamlit', use_container_width=True)


page_names_to_funcs = {
    "—": intro,
    "Analyse par produit": analyse_par_produit,
    "Analyse par commune": analyse_par_commune,
    "Analyse par commune et produit": analyse_par_commune_produit
}

demo_name = st.sidebar.selectbox("Choisir une analyse", page_names_to_funcs.keys())
page_names_to_funcs[demo_name]()
