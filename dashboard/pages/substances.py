# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:47:20 2022

@author: virgi
"""


import pandas as pd
import os
import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.express as px
import seaborn as sns
import os.path
import geopandas as gpd
import folium
from streamlit_folium import folium_static
import sys



###############################################################
# Definir les fonctions specifiques
##############################################################


@st.cache_resource
def load_data_substance():
    """
    Fonction pour charger le jeu de données contenant les données sur les substances
    
    return: a pandas dataframe
    """
    df_2013 = pd.read_feather('data/substances/df_substances_2013.feather')
    df_2014 = pd.read_feather('data/substances/df_substances_2014.feather')
    df_2015 = pd.read_feather('data/substances/df_substances_2015.feather')
    df_2016 = pd.read_feather('data/substances/df_substances_2016.feather')
    df_2017 = pd.read_feather('data/substances/df_substances_2017.feather')
    df_2018 = pd.read_feather('data/substances/df_substances_2018.feather')
    df_2019 = pd.read_feather('data/substances/df_substances_2019.feather')
    df_2020 = pd.read_feather('data/substances/df_substances_2020.feather')
    df_2021 = pd.read_feather('data/substances/df_substances_2021.feather')

    return df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021

@st.cache_resource
def load_data_global():
    """
    Fonction pour charger le jeu de données contenant les données globales sur les substances
    
    return: a pandas dataframe
    """
    df_substances = pd.read_feather('data/substances/df_substances_global.feather')

    return df_substances


@st.cache_resource
def load_geo_data():
    df_geo = gpd.read_file('data/communes-avec-outre-mer.geojson')
    return df_geo


def def_threshold_scale(df):
    # Créer une liste avec toute les quantités pour le produit
    list_toute_les_quantite = df["quantite_par_ha_agricole"].sort_values().tolist()

    # Enlever de la liste 1 % des valeurs les plus grandes (dont les valeurs aberrantes)
    stop = int(len(list_toute_les_quantite) - (len(list_toute_les_quantite) * 0.01))
    list_scale = list_toute_les_quantite[0: stop]

    # Créer une première liste d'intervalle pour la carte
    threshold_scale, step = np.linspace(0, list_scale[-1], 10, retstep=True)

    # changer les 1 % des valeurs les plus grandes par une valeur arbitraire pour la carte
    df.loc[df["quantite_par_ha_agricole"] > threshold_scale[-1],
           "quantite_par_ha_agricole"] =( threshold_scale[-1] + step)
    
    # Ajouter le dernier interval à l'échelle de la carte
    threshold_scale = np.append(threshold_scale, df["quantite_par_ha_agricole"].max())

    return threshold_scale


def show_map(df, df_geo, my_map, select_year=None, threshold_scale=None):
    if select_year:
        df_year = df.loc[df["annee"] == select_year]
    
    else:
        df_year = df
        df_for_map = pd.merge(left=df_year,
                          right=df_geo,
                          how='left',
                          left_on="code_commune_insee",
                          right_on="code")
    
    df_for_map = df_for_map.dropna(subset=["geometry"])
    gdf_for_map = gpd.GeoDataFrame(df_for_map, geometry='geometry')

        
    choro = folium.Choropleth(geo_data=gdf_for_map,
                              name='choropleth',
                              data=gdf_for_map,
                              columns=["code_commune_insee", "quantite_par_ha_agricole"],
                              key_on='feature.properties.code_commune_insee',
                              threshold_scale=threshold_scale,
                              fill_color='RdPu',
                              fill_opacity=1,
                              line_opacity=0.2,
                              legend_name=(" Quantité par hectar de surface agricole (Kg)"),
                              highlight=False).add_to(my_map)

    folium.LayerControl().add_to(my_map)

    choro.geojson.add_child(folium.features.GeoJsonTooltip(['code_commune_insee',
                                                            'quantite_par_commune',
                                                            'Superficie_Territoires_agricoles']))
    folium_static(my_map)  


def intro():
    import streamlit as st

    html_temp = """
                <div style="background-color: gray; padding:5px; border-radius:10px">
                <h1 style="color: white; text-align:center">Achats de substances phytopharmaceutiques en France</h1>
                </div>
                """
    st.markdown(html_temp, unsafe_allow_html=True)
    st.write(" ")

    
    st.sidebar.success("Sélectionner une analyse ci-dessus")

    # Afficher figures de la vue d'ensemble
    with st.spinner("Chargement de la vue d'ensemble..."):
        df_global = load_data_global()

        fig1 = px.bar(df_global,
                      x="Années",
                      y="Nombre_de_substances",
                      color="Classification")

        fig1.update_layout(title="Nombre de substances achetées par année",
                  title_x=0.3,
                  xaxis_title="Années",
                  yaxis_title="Nombre de substances différentes")

        fig2 = px.bar(df_global,
             x="Années",
             y="Quantité",
             color="Classification")

        fig2.update_layout(title="Quantités de substances achetées par année",
                  title_x=0.3,
                  xaxis_title="Années",
                  yaxis_title="Quantités de substances différentes (Kg)")


        tab1, tab2 = st.tabs(["Nombre de substances", "Quantité de substances"])
        with tab1:
            st.plotly_chart(fig1, theme='streamlit', use_container_width=True)
        with tab2:
            st.plotly_chart(fig2, theme='streamlit', use_container_width=True)

        st.write(""":warning: La nomenclature utilisée pour la classification 
               des produits phytopharmaceutiques a changé à partir de 2019""")

        with st.expander(label="🤔 En savoir plus sur les nomenclatures utilisées pour la classification"):
            st.write("Classement des substances en vigueur l'année d'achat considérée au titre\
                      de la redevance pour pollution diffuse")
            st.text('\n')
            st.write("**Nomenclature applicable pour la période 2008-2018**:\n"
                     "- _N minéral_: substance minérale dangereuse pour l'environnement\n"
                     "- _N Organique_: substance organique dangereuse pour l'environnement\n"
                     "- _T, T+, CMR_: substance toxique, très toxique, cancérogène\n"
                     "                mutagène, toxiqload_data_substanceue pour la reproduction\n"
                     "- _Autre_: autre subtance")
            st.text('\n')
            st.write("**Nomenclature applicable à partir de 2019**:\n"
                     "- _CMR_: cancérogénicité, mutagénicité sur les cellules germinales\n"
                     "         ou toxicité pour la reproduction\n"
                     "- _Santé A_: toxicité aiguë de catégorie 1, 2 ou 3, ou toxicité spécifique\n"
                     "             pour certains organes cibles, de catégorie 1, à la suite d'une\n"
                     "             exposition unique ou après une exposition répétée, soit en raison\n"
                     "             de leurs effets sur ou via l'allaitement\n"
                     "- _Env A_: toxicité aiguë pour le milieu aquatique de catégorie 1 ou toxicité\n"
                     "           chronique pour le milieu aquatique de catégorie 1 ou 2\n"
                     "- _Env B_: toxicité chronique pour le milieu aquatique de catégorie 3 ou 4\n"
                     "- _Autre_: autre subtance")
    

        #Pour afficher la carte
        df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_substance()
        df_geo = load_geo_data()

        map_substances = folium.Map(location=[47., 1],
                                    zoom_start=5)
    
        html_temp = """
                    <h3 style="color: white; text-align:center">Répartition géographique des achats de la substance</h3>
                    """
        st.markdown(html_temp, unsafe_allow_html=True)

        list_annees = df_global['Années'].unique().tolist()
        select_year = st.select_slider("Quelle année voulez-vous afficher?",
                                       options=list_annees)

        if select_year == 2013:
            df_2013["quantite_par_ha_agricole"] = (df_2013["quantite_par_commune"]
                                                   / df_2013["Superficie_Territoires_agricoles"])
            df_tmp = df_2013.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)

        if select_year == 2014:
            df_2014["quantite_par_ha_agricole"] = (df_2014["quantite_par_commune"]
                                                   / df_2014["Superficie_Territoires_agricoles"])
            df_tmp = df_2014.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)
        
        if select_year == 2015:
            df_2015["quantite_par_ha_agricole"] = (df_2015["quantite_par_commune"]
                                                   / df_2015["Superficie_Territoires_agricoles"])
            df_tmp = df_2015.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)
        
        if select_year == 2016:
            df_2016["quantite_par_ha_agricole"] = (df_2016["quantite_par_commune"]
                                                   / df_2016["Superficie_Territoires_agricoles"])
            df_tmp = df_2016.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)
        
        if select_year == 2017:
            df_2017["quantite_par_ha_agricole"] = (df_2017["quantite_par_commune"]
                                                   / df_2017["Superficie_Territoires_agricoles"])
            df_tmp = df_2017.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)
            
        if select_year == 2018:
            df_2018["quantite_par_ha_agricole"] = (df_2018["quantite_par_commune"]
                                                   / df_2018["Superficie_Territoires_agricoles"])
            df_tmp = df_2018.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)

        if select_year == 2019:
            df_2019["quantite_par_ha_agricole"] = (df_2019["quantite_par_commune"]
                                                   / df_2019["Superficie_Territoires_agricoles"])
            df_tmp = df_2019.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)

        if select_year == 2020:
            df_2020["quantite_par_ha_agricole"] = (df_2020["quantite_par_commune"]
                                                   / df_2020["Superficie_Territoires_agricoles"])
            df_tmp = df_2020.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)

        if select_year == 2021:
            df_2021["quantite_par_ha_agricole"] = (df_2021["quantite_par_commune"]
                                                   / df_2021["Superficie_Territoires_agricoles"])
            df_tmp = df_2021.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                                  "quantite_par_ha_agricole": "sum",
                                                                  "Superficie_Territoires_agricoles": "first"}).reset_index()
            threshold_scale = def_threshold_scale(df_tmp)
            show_map(df_tmp,
                     df_geo,
                     map_substances,
                     threshold_scale=threshold_scale)
        
        st.write(""":warning: La dernière gradation de l'échelle de couleurs
                 représente les 1% de quantités de substances les plus importantes""")


def analyse_par_substance():
    html_temp = """
                <h1 style="color: white; text-align:center">Analyse par substance</h1>
                """
    st.markdown(html_temp, unsafe_allow_html=True)
    st.write(" ")
    

    st.text_input("Numéro de cas", key="cas")
    cas_id = st.session_state.cas

    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_substance()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    list_quantite = []
    list_annees = []
    list_nom = []
    list_classification = []

    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[list_df[i]["cas"] == cas_id]
        quantite = df_tmp[[ "quantite_par_commune"]].sum().tolist()
        list_quantite.append(quantite[0])
        annees_uniques = df_tmp['annee'].unique().tolist()
        list_annees.append(annees_uniques[0] if annees_uniques else None)
        noms_uniques = df_tmp['substance'].unique().tolist()
        list_nom.append(noms_uniques[-1] if noms_uniques else None)
        classifications_uniques = df_tmp['classification'].unique().tolist()
        list_classification.append(classifications_uniques[-1] if classifications_uniques else None)

    df_fig = pd.DataFrame(list(zip(list_annees,
                                   list_quantite,
                                   list_nom,
                                   list_classification)),
                                   columns =["Années", "Quantité", "Nom", "Classification"])
    
    fig = px.bar(df_fig,
                 x="Années",
                 y="Quantité",
                 color="Classification")
    fig.update_layout(title_text="Quantité de substance (cas = {cas}, nom = {nom} )".format(cas=cas_id,
                                                                                            nom=df_fig["Nom"].unique().tolist()[0]),
                      title_x=0.25,
                      xaxis_title="Années",
                      yaxis_title="Quantité de {nom} (Kg)".format(nom=df_fig["Nom"].unique().tolist()[0]),
                      yaxis_range=[0, df_fig["Quantité"].max()])

    st.plotly_chart(fig, use_container_width=True)

    df_geo = load_geo_data()
    map_substance = folium.Map(location=[47., 1],
                               zoom_start=5)
    
    html_temp = """
                <h3 style="color: white; text-align:center">Répartition géographique des achats de la substance</h3>
                """
    st.markdown(html_temp, unsafe_allow_html=True)

    # Préparer les données pour les cartes
    
    list_annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]
    select_year = st.select_slider("Quelle année voulez-vous afficher?",
                                   options=list_annees)


    if select_year == 2013:
        df_tmp = df_2013.loc[df_2013["cas"] == cas_id]
        df_tmp["quantite_par_ha_agricole"] = (df_tmp["quantite_par_commune"]
                                            / df_tmp["Superficie_Territoires_agricoles"])
        df_tmp1 = df_tmp.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp1)
        show_map(df_tmp1,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    if select_year == 2014:
        df_tmp0 = df_2014.loc[df_2014["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)
    
    if select_year == 2015:
        df_tmp0 = df_2015.loc[df_2015["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    
    if select_year == 2016:
        df_tmp0 = df_2016.loc[df_2016["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    
    if select_year == 2017:
        df_tmp0 = df_2017.loc[df_2017["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    if select_year == 2018:
        df_tmp0 = df_2018.loc[df_2018["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    if select_year == 2019:
        df_tmp0 = df_2019.loc[df_2019["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    if select_year == 2020:
        df_tmp0 = df_2020.loc[df_2020["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)

    if select_year == 2021:
        df_tmp0 = df_2021.loc[df_2021["cas"] == cas_id]
        df_tmp0["quantite_par_ha_agricole"] = (df_tmp0["quantite_par_commune"]
                                            / df_tmp0["Superficie_Territoires_agricoles"])
        df_tmp = df_tmp0.groupby(["code_commune_insee"]).agg({"quantite_par_commune": "sum",
                                                            "quantite_par_ha_agricole": "sum",
                                                            "Superficie_Territoires_agricoles": "first"}).reset_index()
        threshold_scale = def_threshold_scale(df_tmp)
        show_map(df_tmp,
                df_geo,
                map_substance,
                threshold_scale=threshold_scale)


    st.write(""":warning: La dernière gradation de l'échelle de couleurs
             représente les 1% de quantités de substances les plus importantes""")


def analyse_par_commune():
    html_temp = """
                <h1 style="color: white; text-align:center">Analyse par commune</h1>
                """
    st.markdown(html_temp, unsafe_allow_html=True)
    st.write(" ")


    st.text_input("Renseigner le code insee de la commune", key="code")
    code_insee = st.session_state.code

    # Charger les données
    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_substance()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    #Aggreger les données
    list_quantite = []
    list_nb_cas = []
    list_annees = []
    list_nom = []
 
    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[list_df[i]["code_commune_insee"] == code_insee]
        nb_cas = len(df_tmp["cas"].unique().tolist())
        list_nb_cas.append(nb_cas)
        quantite = df_tmp[[ "quantite_par_commune"]].sum().tolist()
        list_quantite.append(quantite[0])
        annees_uniques = df_tmp['annee'].unique().tolist()
        list_annees.append(annees_uniques[0] if annees_uniques else None)
        nom_commune = df_tmp['nom_de_la_commune'].unique().tolist()
        list_nom.append(nom_commune[-1] if nom_commune else "Inconnu")

    df_cas = pd.DataFrame(list(zip(list_annees,
                                   list_nb_cas,
                                   list_quantite,
                                   list_nom)),
                                   columns =["Années", "Nombre de substances", "Quantité", "Commune"])

    fig1 = px.bar(df_cas,
                  x="Années",
                  y="Nombre de substances")
    fig1.update_layout(title_text="Nombre de substance à {nom}".format(nom=df_cas["Commune"].unique().tolist()[0]),
                       title_x=0.25,
                       xaxis_title="Années",
                       yaxis_title="Nombre de substance",
                       yaxis_range=[0, df_cas["Nombre de substances"].max()])

    fig2 = px.bar(df_cas,
                  x="Années",
                  y="Quantité")
    fig2.update_layout(title_text="Quantité de substances à {nom}".format(nom=df_cas["Commune"].unique().tolist()[0]),
                       title_x=0.25,
                       xaxis_title="Années",
                       yaxis_title="Quantité de substances (Kg)",
                       yaxis_range=[0, df_cas["Quantité"].max()])

    tab1, tab2 = st.tabs(["Nombre de substances", "Quantité de substances"])
    with tab1:
        st.plotly_chart(fig1, theme='streamlit', use_container_width=True)
    with tab2:
        st.plotly_chart(fig2, theme='streamlit', use_container_width=True)


def analyse_par_commune_produit():
    st.title("Analyse par commune et substance")


    st.text_input("Numéro de cas", key="cas")
    cas_id = st.session_state.cas

    st.text_input("Code insee", key="code")
    code_insee = st.session_state.code


    # Charger les données
    df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021 = load_data_substance()
    list_df = [df_2013, df_2014, df_2015, df_2016, df_2017, df_2018, df_2019, df_2020, df_2021]
    
    #Aggreger les données
    list_quantite = []
    list_annees = []
    list_nom = []
    list_nom_commune = []

    for i in range(len(list_df)):
        df_tmp = list_df[i].loc[(list_df[i]["code_commune_insee"] == code_insee)
                                & (list_df[i]["cas"] == cas_id)]

        quantite = df_tmp[[ "quantite_par_commune"]].sum().tolist()
        list_quantite.append(quantite[0])
        annees_uniques = df_tmp['annee'].unique().tolist()
        list_annees.append(annees_uniques[0] if annees_uniques else None)
        nom_substance = df_tmp['substance'].unique().tolist()
        list_nom.append(nom_substance[0] if nom_substance else "Inconnu")
        nom_commune = df_tmp['nom_de_la_commune'].unique().tolist()
        list_nom_commune.append(nom_commune[-1] if nom_commune else "Inconnu")


    df_fig = pd.DataFrame(list(zip(list_annees,
                                   list_quantite,
                                   list_nom,
                                   list_nom_commune)),
                                   columns =["Années", "Quantité", "Substance", "Commune"])

    fig = px.bar(df_fig,
                 x="Années",
                 y="Quantité")
    fig.update_layout(title_text="Quantité de {substance} à {commune}".format(substance=df_fig["Substance"].unique().tolist()[0],
                                                                              commune=df_fig["Commune"].unique().tolist()[0]),
                       title_x=0.25,
                       xaxis_title="Années",
                       yaxis_title="Quantité de substances (Kg)",
                       yaxis_range=[0, df_fig["Quantité"].max()])
    st.plotly_chart(fig, theme='streamlit', use_container_width=True)

page_names_to_funcs = {
    "—": intro,
    "Analyse par substance": analyse_par_substance,
    "Analyse par commune": analyse_par_commune,
    "Analyse par commune et produit": analyse_par_commune_produit
}

demo_name = st.sidebar.selectbox("Choisir une analyse", page_names_to_funcs.keys())
page_names_to_funcs[demo_name]()
