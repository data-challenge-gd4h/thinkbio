# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:47:20 2022

@author: virgi
"""

import streamlit as st
import pandas as pd
import plotly.express as px

#############################################

@st.cache
def load_data_produit():
    """
    Fonction pour charger le jeu de données contenant les données sur les produits
    
    return: a pandas dataframe
    """
    df_produits = pd.read_feather("df_produits_2013_2021.feather")
    return df_produits


@st.cache
def load_data_substance():
    """
    Fonction pour charger le jeu de données contenant les données sur les substances
    
    return: a pandas dataframe
    """
    df_substances = pd.read_feather("df_substances_2013_2021.feather")
    return df_substances


def main():
    """Fonction principal pour déterminer ce qui est afficher dans le dashboard
    """

    #############################################################
    # SIDE BAR
    #############################################################

    with st.sidebar:
        html_temp = """
            <p style="font-size: 20px; font-weight: bold; text-align:center">
            Page d'accueil </p>
            """
        st.markdown(html_temp, unsafe_allow_html=True)
    
    ############################################################
    # HOME PAGE
    ############################################################

    html_temp = """
                <div style="background-color: gray; padding:10px; border-radius:10px">
                <h1 style="color: white; text-align:center">Bienvenue sur le Dashboard</h1>
                <h1 style="color: white; text-align:center">Thinkbio</h1>
                </div>
                <p style="font-size: 20px; font-weight: bold; text-align:center">
                ThinkBio est un défi réalisé dans le cadre du challenge Green Data for health 2023</p>
                """
    st.markdown(html_temp, unsafe_allow_html=True)

    with st.expander(label="🤔 Quel est le but de ce défi ?"):
        st.write("Croiser des données sur les pratiques agricoles et l’environnement\
                 pour accélérer la recherche sur l’impact de l’exposition\
                 des résidents ruraux aux produits phytosanitaires, notamment en éducation et santé")
        st.text('\n')


    with st.expander(label="🤔 Quels sont les données utilisées dans ce dashboard ?"):
        st.write("Les figures de ce dashboard sont réalisées à partir des données d'achat de produits phytopharmaceutiques.\
                 A l'origine, ces données se trouvent: \n"
                 "- https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/ \n"
                 "- https://data.eaufrance.fr/jdd/a69c8e76-13e1-4f87-9f9d-1705468b7221 \n"
                 "Pour réaliser les figures, les données de 2013 à 2021 ont été ramené à l'échelle de la commune et aggregé\
                 afin de pouvoir comparer les achats de produits phytopharmaceutiques au cours du temps")
        st.text('\n')
        st.write("Les données d'achat de produits phytopharmaceutiques sont organisées soit par produits soit par substances")



if __name__ == "__main__":
    main()