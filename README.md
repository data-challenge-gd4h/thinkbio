# Challenge GD4H - ThinkBio

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

## ThinkBio

De nombreuses données environnementales existent dans des mailles temporelles et spatiales spécifiques. Ce défi vise à systématiser l’appariement entre bases de données à un niveau cohérent, de manière documentée et en facilitant l’expansion de ce travail d’appariement vers de nouvelles sources de données.

La finalité du projet est de permettre des analyses d’impact de l’utilisation de produits phytosanitaires sur des variables d’éducation et de santé.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=45" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

Pour les données d'achats de produits phytopharmaceutiques, un script a été créé dans le but d'avoir un jeu de données répertoriant les achats de produits phytosanitaires par communes. 
Les jeux de données à disposition rapportent les quantités de produits phytopharmaceutiques achétées par codes postales par année. Le script permet de ramener ces jeux de données à l'échelle de la commune. Lorsqu'un même code postal est associé à plusieurs communes, les quantités de produits/substances ont été calculé de manière à être proportionnelle à la surface agricole des communes ayant le code postal.
Les surfaces agricoles de chaque commune ont été extraites à partir les données Corine Land Cover séparant les surfaces des communes en 5 catégories (https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0).

Une analyse exploratoire des données de 2021 a été réalisé dans le notebook Données_phyto_EDA. Le notebook EDA_produits_over_year fait une brève exploration du fichier créé avec le script et regroupant les données de 2013 à 2021.

### **Installation**

La source des données d'achats des produits phytopharmaceutiques, l'environnement utilisé ainsi que les dépendances utilisées sont décrites dans le fichier [Guide d'installation](/INSTALL.md)

### **Utilisation**

Une fois l'environnement installé, la solution envisagée pour les produits d'achats de produits phytopharmaceutiques correspond a un [script](/src/script.py) dont certains paramètres doivent être ajustés. Ceux-ci se situent vers la fin du script en dessous de "if __name__ == "__main__":" et sont:
- le chemin d'accés au répertoire local contenant le fichier zip téléchargé
- "substances" ou "produits" afin d'indiquer si le jeu de données devant être créé concerne les produits phytopharmaceutiques ou les substances qui les composent.
- le chemin d'accés au répertoire contenant un fichier csv avec les surfaces agricoles par communes
- le chemin d'accés au répertoire où le jeu de données créé doit être sauvegardé
- "True" or "False" pour update afin d'indiquer si le jeu de données est créé pour une année (False) ou si c'est pour mettre à jour un fichier déjà existant (True). Si update = True, il est nécessaire de préciser le chemin d'accès au répetoire contenant le fichier à mettre à jour
- le format et le nom du fichier qui sera créé. Il est possible de créer des fichiers csv ou feather. Etant donnée la taille des fichiers créés, les fichiers au format feather seront plus facilement manipulable et seront plus leger.

Une fois tous ces paramètres indiquer, il suffit de faire tourner le script.

La webapp streamlit se lance depuis le dossier dashboard
```bash
cd dashboard
streamlit run dashboard.py
```

### **Point sur les branches**

La branche principale est équivalente à la branche **donnees_phyto**, finalisée et utilisable en l'état. Les autres branches **donnees_meteo** et **donnees_spatiales** contiennent des codes non finalisés et inutilisables en l'état. 

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
